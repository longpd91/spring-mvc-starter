package com.longpd.springmvcstarter.control;

import com.longpd.springmvcstarter.service.HelloWorldService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

/**
 * Created by hoang on 6/29/2017.
 */
@Controller
@RequestMapping("/springmvc")
public class HelloWorldController {
    private final Logger logger = LoggerFactory.getLogger(HelloWorldController.class);
    private final HelloWorldService helloWorldService;

    @Autowired
    public HelloWorldController(HelloWorldService helloWorldService) {
        this.helloWorldService = helloWorldService;
    }
//    @RequestMapping("/hello")
//    public String hello(Model model) {
//        System.out.println("hello");
//        model.addAttribute("greeting", "Hello Spring MVC");
//        return "helloworld";
//    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Map<String, Object> model) {

        logger.debug("index() is executed!");
        System.out.println("index() is executed!");
        model.put("greeting", helloWorldService.getTitle("32"));
        model.put("msg", helloWorldService.getDesc());

        return "helloworld";
    }

}
